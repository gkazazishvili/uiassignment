package com.example.new_assignment

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Patterns
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import com.example.new_assignment.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private var showPass = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

    private fun init(){
      listeners()
    }


    private fun listeners(){
        binding.hiddenPass.setOnClickListener {
            showPass = !showPass
            showPassword(showPass)
        }
        showPassword(showPass)

        binding.signIn.setOnClickListener {
            emailChecker()
        }
    }

    private fun showPassword(isShown: Boolean){
        if(isShown){
            binding.password.transformationMethod = HideReturnsTransformationMethod.getInstance()
            binding.hiddenPass.setImageResource(R.drawable.ic_component_1___12)
        } else{
            binding.password.transformationMethod = PasswordTransformationMethod.getInstance()
            binding.hiddenPass.setImageResource(R.drawable.ic_component_1___11)
        }
        binding.password.setSelection(binding.password.text.toString().length)
    }

    private fun emailChecker(){
        var emailInput:String = binding.email.text.toString()

        if(!emailInput.isEmpty() && Patterns.EMAIL_ADDRESS.matcher(emailInput).matches()){
            Toast.makeText(this, "Email Validated Successfully", Toast.LENGTH_SHORT).show()
        } else{
            Toast.makeText(this, "Email Is Not Valid", Toast.LENGTH_SHORT).show()

        }
    }
}